 Command line instructions
Git global setup

git config --global user.name "Md Raihan"
git config --global user.email "sayedahmed7921543@gmail.com"

Create a new repository

git clone https://gitlab.com/AhmedRaihan/Raihan_seid_B69_session-5.git
cd Raihan_seid_B69_session-5
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/AhmedRaihan/Raihan_seid_B69_session-5.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/AhmedRaihan/Raihan_seid_B69_session-5.git
git push -u origin --all
git push -u origin --tags

